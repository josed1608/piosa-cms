import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SocialActorTableComponent } from './social-actor-table.component';

describe('SocialActorTableComponent', () => {
  let component: SocialActorTableComponent;
  let fixture: ComponentFixture<SocialActorTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SocialActorTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SocialActorTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
