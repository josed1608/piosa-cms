import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {SocialActorService} from '../social-actor.service';

@Component({
    selector: 'app-social-actor-table',
    templateUrl: './social-actor-table.component.html',
    styleUrls: ['./social-actor-table.component.scss']
})
export class SocialActorTableComponent implements OnInit {

    content = [];

    constructor(private router: Router,
                private route: ActivatedRoute,
                private socialActorService: SocialActorService) {
    }

    ngOnInit() {
        this.getData();
    }

    getData() {
        this.socialActorService.getSocialActors()
            .subscribe(
                (results: any[]) => {
                    this.content = results;

                });
    }

    onClickEdit(object: any) {
        this.socialActorService.selectedObject = object;
        this.router.navigate(['/dashboard/social-actor/edit'], {
            relativeTo: this.route,
            queryParams: {id: object.id},
            // fragment: 'loading'
        });
    }

    onClickErase(object: any) {
        this.socialActorService.deleteSocialActor(object)
            .subscribe(
                (response) => {
                    console.log(response);
                    this.getData();
                },
                (error) => {
                    this.content = [];
                    console.log(error);
                }
            );
    }


    onClickCreate() {
        this.socialActorService.selectedObject = null;
        this.router.navigate(['/dashboard/social-actor/new'], {
            relativeTo: this.route,
        });
    }
}
