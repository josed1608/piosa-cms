import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SocialActorFormComponent } from './social-actor-form.component';

describe('SocialActorFormComponent', () => {
  let component: SocialActorFormComponent;
  let fixture: ComponentFixture<SocialActorFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SocialActorFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SocialActorFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
