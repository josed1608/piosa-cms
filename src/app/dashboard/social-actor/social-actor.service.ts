import {Injectable, Input} from '@angular/core';
import {Headers, Http, RequestOptions, Response} from '@angular/http';
import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';
import {AppConstants} from '../../app-constants';

@Injectable()
export class SocialActorService {

    @Input() selectedObject: any;

    constructor(private http: Http) {
    }

    createSocialActor(socialActor: any) {
        console.log('Create');
        const headers = new Headers({'Content-Type': 'application/json'});

        return this.http.post(AppConstants.API_ENDPOINT + 'socialActor/create',
            socialActor,
            {headers: headers});
    }

    getSocialActors() {
        console.log('Read');
        return this.http.get(AppConstants.API_ENDPOINT + 'socialActor/findAll')
            .map(
                (response: Response) => {
                    return response.json();
                }
            )
            .catch(
                (error: Response) => {
                    return Observable.throw('Something went wrong' + error);
                }
            );
    }

    getById(objId: string) {
        return this.http.get(AppConstants.API_ENDPOINT + 'socialActor/findById/' + objId)
            .map(
                (response: Response) => {
                    return response.json();
                }
            )
            .catch(
                (error: Response) => {
                    return Observable.throw('Something went wrong' + error);
                }
            );
    }

    updateSocialActor(socialActor: any) {
        console.log('Update');
        const headers = new Headers({'Content-Type': 'application/json'});
        return this.http.put(AppConstants.API_ENDPOINT + 'socialActor/update',
            socialActor,
            {headers: headers});
    }

    deleteSocialActor(obj: any) {
        console.log('Delete');
        const headers = new Headers({'Content-Type': 'application/json'});
        return this.http.delete(AppConstants.API_ENDPOINT + 'socialActor/delete',
            new RequestOptions({
                headers: headers,
                body: obj
            }));
    }

}
