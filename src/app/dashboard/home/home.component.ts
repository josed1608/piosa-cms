import {Component, OnInit} from '@angular/core';
import {NewsService} from '../news/news.service';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {

    content = [];

    ngOnInit() {
        this.loadNewsFeeds();
    }

    constructor(private newsService: NewsService) {
    }


    loadNewsFeeds() {
        this.newsService.getFacebook()
            .subscribe(
                (results: any[]) => {
                    this.content = results;
                });
    }


    onUpdateNews() {
        this.newsService.updateFromFacebook()
            .subscribe(
                (res: any[]) => {
                    this.loadNewsFeeds();
                });
    }

}
