import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AdvanceService} from '../advance.service';

@Component({
    selector: 'app-advance-table',
    templateUrl: './advance-table.component.html',
    styleUrls: ['./advance-table.component.scss']
})
export class AdvanceTableComponent implements OnInit {

    content = [];

    constructor(private router: Router,
                private route: ActivatedRoute,
                private advanceService: AdvanceService) {
    }

    ngOnInit() {
        this.getData();
    }

    getData() {
        this.advanceService.getAdvances()
            .subscribe(
                (results: any[]) => {
                    this.content = results;

                });
    }

    onClickEdit(object: any) {
        this.advanceService.selectedObject = object;
        this.router.navigate(['/dashboard/advance/edit'], {
            relativeTo: this.route,
            queryParams: {id: object.id},
            // fragment: 'loading'
        });
    }

    onClickErase(object: any) {
        this.advanceService.deleteAdvance(object)
            .subscribe(
                (response) => {
                    console.log(response);
                    this.getData();
                },
                (error) => {
                    this.content = [];
                    console.log(error);
                }
            );
    }


    onClickCreate() {
        this.advanceService.selectedObject = null;
        this.router.navigate(['/dashboard/advance/new'], {
            relativeTo: this.route,
        });
    }

}
