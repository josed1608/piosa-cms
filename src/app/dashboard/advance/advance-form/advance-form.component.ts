import {Component, ElementRef, OnInit, ViewContainerRef} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToastsManager} from 'ng2-toastr';
import {Subscription} from 'rxjs/Subscription';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {AdvanceService} from '../advance.service';
import {ProyectService} from '../../proyect/proyect.service';

@Component({
    selector: 'app-advance-form',
    templateUrl: './advance-form.component.html',
    styleUrls: ['./advance-form.component.scss']
})
export class AdvanceFormComponent implements OnInit {

    paramsSubscription: Subscription;
    advanceForm: FormGroup;

    currentProy: any;
    proyects = [];

    object: any;
    loading = false;
    pageType: string;

    creation_date: any;
    modif_date: any;

    constructor(public toastr: ToastsManager,
                vRef: ViewContainerRef,
                private router: Router,
                private element: ElementRef,
                private route: ActivatedRoute,
                private proyectService: ProyectService,
                private advanceService: AdvanceService,
                private formBuilder: FormBuilder) {

        this.toastr.setRootViewContainerRef(vRef);
    }

    ngOnInit() {

        this.advanceForm = this.formBuilder.group({
            title: ['', [Validators.required]],
            description: ['', Validators.required],
            proyect_id: ['', Validators.required],
            active: ['', Validators.nullValidator]
        });

        this.loadProyects();
        this.object = this.advanceService.selectedObject;
        if (this.object != null) {
            this.loadFromObject();
        }

        this.route.queryParams
            .subscribe(
                (queryParams: Params) => {
                    const id = queryParams['id'];
                    console.log(id);
                    if (id != null) {
                        this.advanceService.getById(id).subscribe(
                            (data: any) => {
                                if (data != null && data.length > 0) {
                                    this.object = data[0];
                                    console.log(this.object);
                                    this.loadFromObject();
                                }
                            });
                    }
                });

        this.paramsSubscription = this.route.params
            .subscribe(
                (params: Params) => {
                    this.pageType = params['id'];
                }
            );


    }


    loadProyects() {
        this.proyectService.getProyects()
            .subscribe(
                (results: any[]) => {
                    this.proyects = results;
                    if (this.proyects.length > 0) {
                        if (this.currentProy == null) {
                            this.currentProy = this.proyects[0].id;
                        }
                    }

                });
    }

    loadFromObject() {

        if (this.object.id != null) {
            this.advanceForm.patchValue({
                'id': this.object.id
            });
        }

        if (this.object.title != null) {
            this.advanceForm.patchValue({
                'title': this.object.title
            });
        }

        if (this.object.description != null) {
            this.advanceForm.patchValue({
                'description': this.object.description
            });
        }

        if (this.object.active != null) {
            this.advanceForm.patchValue({
                'active': this.object.active
            });
        }

        const sdate = new Date(this.object.start_date);
        if (this.object.start_date != null) {
            this.advanceForm.controls['start_date'].setValue(sdate.toISOString().substring(0, 10));
        }

        const edate = new Date(this.object.end_date);
        if (this.object.end_date != null) {
            this.advanceForm.controls['end_date'].setValue(edate.toISOString().substring(0, 10));
        }

        if (this.object.creation_date != null) {
            this.creation_date = new Date(this.object.creation_date);
        }

        if (this.object.modif_date != null) {
            this.modif_date = new Date(this.object.modif_date);
        }

        if (this.object.type != null) {
            this.advanceForm.patchValue({
                'type': this.object.type
            });
            this.currentProy = this.object.type;
        }
    }

    onCreate() {
        const object = this.advanceForm.value;
        console.log(object);
        this.advanceService.createAdvance(object)
            .subscribe(
                (response) => {
                    console.log(response);
                    this.object = response;

                    this.router.navigate(['../'], {
                        relativeTo: this.route,
                    });
                },
                (error) => {
                    this.toastr.error('Ha ocurrido un error!', 'Ocurrió un error!');
                    console.log(error);
                }
            );
    }


    onUpdate() {
        // console.log(this.articleForm.value);
        const object = this.advanceForm.value;
        object.id = this.object.id;
        console.log(object);
        this.advanceService.updateAdvance(object)
            .subscribe(
                (response) => {
                    console.log(response);
                    this.router.navigate(['../'], {
                        relativeTo: this.route,
                        queryParams: {success: true},
                    });
                },
                (error) => {
                    this.toastr.error('Ha ocurrido un error!', 'Ocurrió un error!');
                    console.log(error);
                }
            );
    }

}
