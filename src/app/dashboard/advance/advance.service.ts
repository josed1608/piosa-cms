import {Injectable, Input} from '@angular/core';
import {Headers, Http, RequestOptions, Response} from '@angular/http';
import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';
import {AppConstants} from '../../app-constants';

@Injectable()
export class AdvanceService {

    @Input() selectedObject: any;

    constructor(private http: Http) {
    }

    createAdvance(advance: any) {
        console.log('Create');
        const headers = new Headers({'Content-Type': 'application/json'});

        return this.http.post(AppConstants.API_ENDPOINT + 'advance/create',
            advance,
            {headers: headers});
    }

    getAdvances() {
        console.log('Read');
        return this.http.get(AppConstants.API_ENDPOINT + 'advance/findAll')
            .map(
                (response: Response) => {
                    return response.json();
                }
            )
            .catch(
                (error: Response) => {
                    return Observable.throw('Something went wrong' + error);
                }
            );
    }

    getById(objId: string) {
        return this.http.get(AppConstants.API_ENDPOINT + 'advance/findById/' + objId)
            .map(
                (response: Response) => {
                    return response.json();
                }
            )
            .catch(
                (error: Response) => {
                    return Observable.throw('Something went wrong' + error);
                }
            );
    }

    updateAdvance(advance: any) {
        console.log('Update');
        const headers = new Headers({'Content-Type': 'application/json'});
        return this.http.put(AppConstants.API_ENDPOINT + 'advance/update',
            advance,
            {headers: headers});
    }

    deleteAdvance(obj: any) {
        console.log('Delete');
        const headers = new Headers({'Content-Type': 'application/json'});
        return this.http.delete(AppConstants.API_ENDPOINT + 'advance/delete',
            new RequestOptions({
                headers: headers,
                body: obj
            }));
    }

}
