import {Injectable, Input} from '@angular/core';
import {Headers, Http, RequestOptions, Response} from '@angular/http';
import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';
import {AppConstants} from '../../app-constants';

@Injectable()
export class UserService {

    @Input() selectedObject: any;

    constructor(private http: Http) {
    }

    doLogin(user: any) {
        const headers = new Headers({'Content-Type': 'application/json'});
        return this.http.post(AppConstants.API_ENDPOINT + 'user/login',
            user,
            {headers: headers})
            .map(
                (response: Response) => {
                    const data = response.json();
                    // localStorage.setItem('token', content.token);
                    localStorage.setItem('user', JSON.stringify(data));
                    return data;
                }
            )
            .catch(
                (error: Response) => {
                    return Observable.throw(error);
                }
            );
    }

    createUser(user: any) {
        console.log('Create');
        const headers = new Headers({'Content-Type': 'application/json'});

        return this.http.post(AppConstants.API_ENDPOINT + 'user/create',
            user,
            {headers: headers});
    }

    getUsers() {
        console.log('Read');
        return this.http.get(AppConstants.API_ENDPOINT + 'user/findAll')
            .map(
                (response: Response) => {
                    return response.json();
                }
            )
            .catch(
                (error: Response) => {
                    return Observable.throw('Something went wrong' + error);
                }
            );
    }

    getById(objId: string) {
        console.log('Read');
        console.log(objId);
        return this.http.get(AppConstants.API_ENDPOINT + 'user/findById/' + objId)
            .map(
                (response: Response) => {
                    return response.json();
                }
            )
            .catch(
                (error: Response) => {
                    return Observable.throw('Something went wrong' + error);
                }
            );
    }

    getByUser(id: string) {
        console.log('Read');
        return this.http.get(AppConstants.API_ENDPOINT + 'user/findByUser/' + id)
            .map(
                (response: Response) => {
                    return response.json();
                }
            )
            .catch(
                (error: Response) => {
                    return Observable.throw('Something went wrong' + error);
                }
            );
    }


    updateUser(user: any) {
        console.log('Update');
        const headers = new Headers({'Content-Type': 'application/json'});
        return this.http.put(AppConstants.API_ENDPOINT + 'user/update',
            user,
            {headers: headers});
    }

    deleteUser(obj: any) {
        console.log('Delete');
        const headers = new Headers({'Content-Type': 'application/json'});
        return this.http.delete(AppConstants.API_ENDPOINT + 'user/delete',
            new RequestOptions({
                headers: headers,
                body: obj
            }));
    }

}
