import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../user.service';

@Component({
    selector: 'app-user-table',
    templateUrl: './user-table.component.html',
    styleUrls: ['./user-table.component.scss']
})
export class UserTableComponent implements OnInit {

    content = [];

    constructor(private router: Router,
                private route: ActivatedRoute,
                private userService: UserService) {
    }

    ngOnInit() {
        this.getData();
    }

    getData() {
        this.userService.getUsers()
            .subscribe(
                (results: any[]) => {
                    this.content = results;

                });
    }

    onClickEdit(object: any) {
        this.userService.selectedObject = object;
        this.router.navigate(['/dashboard/user/edit'], {
            relativeTo: this.route,
            queryParams: {id: object.id},
            // fragment: 'loading'
        });
    }

    onClickErase(object: any) {
        this.userService.deleteUser(object)
            .subscribe(
                (response) => {
                    console.log(response);
                    this.getData();
                },
                (error) => {
                    console.log(error);
                    this.content = [];
                }
            );
    }


    onClickCreate() {
        this.userService.selectedObject = null;
        this.router.navigate(['/dashboard/user/new'], {
            relativeTo: this.route,
        });
    }

}
