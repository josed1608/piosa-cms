import {Component, ElementRef, OnInit, ViewContainerRef} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToastsManager} from 'ng2-toastr';
import {Subscription} from 'rxjs/Subscription';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {EntityService} from '../../admin-user/entity.service';
import {UserService} from '../user.service';

@Component({
    selector: 'app-user-form',
    templateUrl: './user-form.component.html',
    styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {

    paramsSubscription: Subscription;
    userForm: FormGroup;

    object: any;
    loading = false;
    pageType: string;

    constructor(public toastr: ToastsManager,
                vRef: ViewContainerRef,
                private router: Router,
                private element: ElementRef,
                private route: ActivatedRoute,
                private userService: UserService,
                private entityService: EntityService,
                private formBuilder: FormBuilder) {

        this.toastr.setRootViewContainerRef(vRef);
    }

    ngOnInit() {
        this.userForm = this.formBuilder.group({
            name: ['', [Validators.required]],
            email: ['', Validators.required],
            password: ['', [Validators.required]],
        });


        this.object = this.userService.selectedObject;
        if (this.object != null) {
            this.loadFromObject();
        }

        this.route.queryParams
            .subscribe(
                (queryParams: Params) => {
                    const id = queryParams['id'];
                    console.log(id);
                    if (id != null) {
                        this.userService.getById(id).subscribe(
                            (data: any) => {
                                if (data != null && data.length > 0) {
                                    this.object = data[0];
                                    console.log(this.object);
                                    this.loadFromObject();
                                }
                            });
                    }
                });

        this.paramsSubscription = this.route.params
            .subscribe(
                (params: Params) => {
                    this.pageType = params['id'];
                }
            );


    }

    loadFromObject() {

        if (this.object.name != null) {
            this.userForm.patchValue({
                'name': this.object.name
            });
        }

        if (this.object.email != null) {
            this.userForm.patchValue({
                'email': this.object.email
            });
        }

        if (this.object.password != null) {
            this.userForm.patchValue({
                'password': this.object.password
            });
        }

    }

    onCreate() {
        const object = this.userForm.value;
        console.log(object);
        this.userService.createUser(object)
            .subscribe(
                (response) => {
                    console.log(response);
                    this.object = response;

                    this.router.navigate(['../'], {
                        relativeTo: this.route,
                    });
                },
                (error) => {
                    this.toastr.error('Ha ocurrido un error!', 'Ocurrió un error!');
                    console.log(error);
                }
            );
    }


    onUpdate() {
        // console.log(this.articleForm.value);
        const object = this.userForm.value;
        object.id = this.object.id;
        console.log(object);
        this.userService.updateUser(object)
            .subscribe(
                (response) => {
                    console.log(response);
                    this.router.navigate(['../'], {
                        relativeTo: this.route,
                        queryParams: {success: true},
                    });
                },
                (error) => {
                    this.toastr.error('Ha ocurrido un error!', 'Ocurrió un error!');
                    console.log(error);
                }
            );
    }


}
