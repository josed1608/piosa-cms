import {Injectable, Input} from '@angular/core';
import {Headers, Http, RequestOptions, Response} from '@angular/http';
import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';
import {AppConstants} from '../../app-constants';

@Injectable()
export class ProyectTypeService {

    @Input() selectedObject: any;

    constructor(private http: Http) {
    }

    createProyectType(proyectType: any) {
        console.log('Create');
        const headers = new Headers({'Content-Type': 'application/json'});

        return this.http.post(AppConstants.API_ENDPOINT + 'proyectType/create',
            proyectType,
            {headers: headers});
    }

    getProyectTypes() {
        console.log('Read');
        return this.http.get(AppConstants.API_ENDPOINT + 'proyectType/findAll')
            .map(
                (response: Response) => {
                    return response.json();
                }
            )
            .catch(
                (error: Response) => {
                    return Observable.throw('Something went wrong' + error);
                }
            );
    }

    getById(objId: string) {
        return this.http.get(AppConstants.API_ENDPOINT + 'proyectType/findById/' + objId)
            .map(
                (response: Response) => {
                    const data = response.json();
                    return data.content;
                }
            )
            .catch(
                (error: Response) => {
                    return Observable.throw('Something went wrong' + error);
                }
            );
    }

    updateProyectType(proyectType: any) {
        console.log('Update');
        const headers = new Headers({'Content-Type': 'application/json'});
        return this.http.put(AppConstants.API_ENDPOINT + 'proyectType/update',
            proyectType,
            {headers: headers});
    }

    deleteProyectType(obj: any) {
        console.log('Delete');
        const headers = new Headers({'Content-Type': 'application/json'});
        return this.http.delete(AppConstants.API_ENDPOINT + 'proyectType/delete',
            new RequestOptions({
                headers: headers,
                body: obj
            }));
    }

}
