import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProyectTableComponent } from './proyect-table.component';

describe('ProyectTableComponent', () => {
  let component: ProyectTableComponent;
  let fixture: ComponentFixture<ProyectTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProyectTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProyectTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
