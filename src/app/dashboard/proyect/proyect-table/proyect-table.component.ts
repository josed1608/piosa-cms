import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ProyectService} from '../proyect.service';

@Component({
    selector: 'app-proyect-table',
    templateUrl: './proyect-table.component.html',
    styleUrls: ['./proyect-table.component.scss']
})
export class ProyectTableComponent implements OnInit {

    content = [];

    constructor(private router: Router,
                private route: ActivatedRoute,
                private proyectService: ProyectService) {
    }

    ngOnInit() {
        this.getData();
    }

    getData() {
        this.proyectService.getProyects()
            .subscribe(
                (results: any[]) => {
                    this.content = results;

                });
    }

    onClickEdit(object: any) {
        this.proyectService.selectedObject = object;
        this.router.navigate(['/dashboard/proyect/edit'], {
            relativeTo: this.route,
            queryParams: {id: object.id},
            // fragment: 'loading'
        });
    }

    onClickErase(object: any) {
        this.proyectService.deleteProyect(object)
            .subscribe(
                (response) => {
                    console.log(response);
                    this.getData();
                },
                (error) => {
                    console.log(error);
                    this.content = [];
                }
            );
    }


    onClickCreate() {
        this.proyectService.selectedObject = null;
        this.router.navigate(['/dashboard/proyect/new'], {
            relativeTo: this.route,
        });
    }

}
