import {Component, ElementRef, OnInit, ViewContainerRef} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToastsManager} from 'ng2-toastr';
import {Subscription} from 'rxjs/Subscription';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {ProyectService} from '../proyect.service';
import {ProyectTypeService} from '../proyect-type.service';
import * as AWS from 'aws-sdk';

@Component({
    selector: 'app-proyect-form',
    templateUrl: './proyect-form.component.html',
    styleUrls: ['./proyect-form.component.scss']
})
export class ProyectFormComponent implements OnInit {

    paramsSubscription: Subscription;
    proyectForm: FormGroup;
    s3: any;
    proyectTypes = [];

    object: any;
    currentType: any;
    loading = false;
    pageType: string;

    creation_date: any;
    modif_date: any;

    constructor(public toastr: ToastsManager,
                vRef: ViewContainerRef,
                private router: Router,
                private element: ElementRef,
                private route: ActivatedRoute,
                private proyectService: ProyectService,
                private proyectTypeService: ProyectTypeService,
                private formBuilder: FormBuilder) {

        this.toastr.setRootViewContainerRef(vRef);

        this.s3 = new AWS.S3({
            params: {
                Bucket: 'piosa-storage-bucket'
            }
        });

        const creds = new AWS.Credentials('AKIAIJ4PSDOZABQ3OTPQ', 'vfeGuLB4izhClVcsgXJXWkqHMLwlyzHhEe1b7pf5', '');
        this.s3.config.update({
            region: 'us-west-2',
            credentials: creds
        });

    }

    ngOnInit() {

        this.proyectForm = this.formBuilder.group({
            id: ['', [Validators.required]],
            name: ['', [Validators.required]],
            description: ['', Validators.nullValidator],
            active: ['', Validators.nullValidator],
            start_date: ['', Validators.nullValidator],
            end_date: ['', Validators.nullValidator],
            type: ['', Validators.required],
            img_url: ['', Validators.nullValidator],

        });

        this.loadProyectTypes();
        this.object = this.proyectService.selectedObject;
        if (this.object != null) {
            this.loadFromObject();
        }

        this.route.queryParams
            .subscribe(
                (queryParams: Params) => {
                    const id = queryParams['id'];
                    console.log(id);
                    if (id != null) {
                        this.proyectService.getById(id).subscribe(
                            (data: any) => {
                                if (data != null && data.length > 0) {
                                    this.object = data[0];
                                    console.log(this.object);
                                    this.loadFromObject();
                                }
                            });
                    }
                });

        this.paramsSubscription = this.route.params
            .subscribe(
                (params: Params) => {
                    this.pageType = params['id'];
                }
            );


    }


    loadProyectTypes() {
        this.proyectTypeService.getProyectTypes()
            .subscribe(
                (results: any[]) => {
                    this.proyectTypes = results;
                    if (this.proyectTypes.length > 0) {
                        if (this.currentType == null) {
                            this.currentType = this.proyectTypes[0].id;
                        }
                    }

                });
    }

    loadFromObject() {

        if (this.object.id != null) {
            this.proyectForm.patchValue({
                'id': this.object.id
            });
        }

        if (this.object.name != null) {
            this.proyectForm.patchValue({
                'name': this.object.name
            });
        }

        if (this.object.description != null) {
            this.proyectForm.patchValue({
                'description': this.object.description
            });
        }

        if (this.object.active != null) {
            this.proyectForm.patchValue({
                'active': this.object.active
            });
        }

        // if (this.object.start_date != null) {
        //     if (this.object.start_date.length > 0) {
        //         const sdate = new Date(this.object.start_date);
        //         if (this.object.start_date != null) {
        //             this.proyectForm.controls['start_date'].setValue(sdate.toISOString().substring(0, 10));
        //         }
        //     }
        // }
        //
        // if (this.object.end_date != null) {
        //     if (this.object.end_date.length > 0) {
        //         const edate = new Date(this.object.end_date);
        //         if (this.object.end_date != null) {
        //             this.proyectForm.controls['end_date'].setValue(edate.toISOString().substring(0, 10));
        //         }
        //     }
        // }

        if (this.object.creation_date != null) {
            this.creation_date = new Date(this.object.creation_date);
        }

        if (this.object.modif_date != null) {
            this.modif_date = new Date(this.object.modif_date);
        }

        if (this.object.type != null) {
            this.proyectForm.patchValue({
                'type': this.object.type
            });
            this.currentType = this.object.type;
        }

        if (this.object.img_url != null) {
            this.proyectForm.patchValue({
                'img_url': this.object.img_url
            });

            const mImage = this.element.nativeElement.querySelector('#proyImage');
            mImage.src = this.object.img_url;
            mImage.hidden = false;

        } else {

            const mImage = this.element.nativeElement.querySelector('#proyImage');
            mImage.hidden = true;
        }
    }

    onCreate() {
        const object = this.proyectForm.value;
        console.log(this.currentType);
        console.log(object);
        this.proyectService.createProyect(object)
            .subscribe(
                (response) => {
                    console.log(response);
                    this.object = response;

                    this.router.navigate(['../'], {
                        relativeTo: this.route,
                    });
                },
                (error) => {
                    this.toastr.error('Ha ocurrido un error!', 'Ocurrió un error!');
                    console.log(error);
                }
            );
    }


    onUpdate() {
        // console.log(this.articleForm.value);
        const object = this.proyectForm.value;
        object.id = this.object.id;
        console.log(object);
        this.proyectService.updateProyect(object)
            .subscribe(
                (response) => {
                    console.log(response);
                    this.router.navigate(['../'], {
                        relativeTo: this.route,
                        queryParams: {success: true},
                    });
                },
                (error) => {
                    this.toastr.error('Ha ocurrido un error!', 'Ocurrió un error!');
                    console.log(error);
                }
            );
    }

    onImageChange(event) {
        const file = event.target.files[0];
        const res = this.uploadFile(file);
        console.log('El resultado es ' + res);
    }

    uploadFile(file: any) {
        console.log(file);

        this.s3.upload({
            Key: file.name,
            Body: file,
            ContentType: file.type,
            ACL: 'public-read'

        }, (err: any, data: any) => {
            if (err) {
                console.log(err);
                // alert(err.message);
            } else {
                console.log(data);
                // alert('Imagen subida');
                this.proyectForm.patchValue({
                    'img_url': data.Location
                });

                const image = this.element.nativeElement.querySelector('#proyImage');
                image.src = data.Location;
                image.hidden = false;

            }
        });
    }


}
