import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {EventService} from '../event.service';

@Component({
  selector: 'app-event-table',
  templateUrl: './event-table.component.html',
  styleUrls: ['./event-table.component.scss']
})
export class EventTableComponent implements OnInit {

    content = [];

    constructor(private router: Router,
                private route: ActivatedRoute,
                private eventService: EventService) {
    }

    ngOnInit() {
        this.getData();
    }

    getData() {
        this.eventService.getEvents()
            .subscribe(
                (results: any[]) => {
                    this.content = results;

                });
    }

    onClickEdit(object: any) {
        this.eventService.selectedObject = object;
        this.router.navigate(['/dashboard/event/edit'], {
            relativeTo: this.route,
            queryParams: {id: object.id},
            // fragment: 'loading'
        });
    }

    onClickErase(object: any) {
        this.eventService.deleteEvent(object)
            .subscribe(
                (response) => {
                    console.log(response);
                    this.getData();
                },
                (error) => {
                    console.log(error);
                }
            );
    }


    onClickCreate() {
        this.eventService.selectedObject = null;
        this.router.navigate(['/dashboard/event/new'], {
            relativeTo: this.route,
        });
    }
}
