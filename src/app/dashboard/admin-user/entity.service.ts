import {Injectable, Input} from '@angular/core';
import {Headers, Http, RequestOptions, Response} from '@angular/http';
import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';
import {AppConstants} from '../../app-constants';

@Injectable()
export class EntityService {

  @Input() selectedObject: any;

  constructor(private http: Http) {
  }

  createEntity(entity: any) {
    console.log('Create');
    const headers = new Headers({'Content-Type': 'application/json'});

    return this.http.post(AppConstants.API_ENDPOINT + 'entity/create',
      entity,
      {headers: headers});
  }

  getEntities() {
    console.log('Read');
    return this.http.get(AppConstants.API_ENDPOINT + 'entity/findAll')
      .map(
        (response: Response) => {
          return response.json();
        }
      )
      .catch(
        (error: Response) => {
          return Observable.throw('Something went wrong' + error);
        }
      );
  }

  getById(objId: string) {
    return this.http.get(AppConstants.API_ENDPOINT + 'entity/findById/' + objId)
      .map(
        (response: Response) => {
          const data = response.json();
          return data.content;
        }
      )
      .catch(
        (error: Response) => {
          return Observable.throw('Something went wrong' + error);
        }
      );
  }

  updateEntity(entity: any) {
    console.log('Update');
    const headers = new Headers({'Content-Type': 'application/json'});
    return this.http.put(AppConstants.API_ENDPOINT + 'entity/update',
      entity,
      {headers: headers});
  }

  deleteEntity(obj: any) {
    console.log('Delete');
    const headers = new Headers({'Content-Type': 'application/json'});
    return this.http.delete(AppConstants.API_ENDPOINT + 'entity/delete',
      new RequestOptions({
        headers: headers,
        body: obj
      }));
  }

}
