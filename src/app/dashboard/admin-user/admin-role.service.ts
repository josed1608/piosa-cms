import {Injectable, Input} from '@angular/core';
import {Headers, Http, RequestOptions, Response} from '@angular/http';
import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';
import {AppConstants} from '../../app-constants';

@Injectable()
export class AdminRoleService {

  @Input() selectedObject: any;

  constructor(private http: Http) {
  }

  createAdminRole(adminRole: any) {
    console.log('Create');
    const headers = new Headers({'Content-Type': 'application/json'});

    return this.http.post(AppConstants.API_ENDPOINT + 'adminRole/create',
      adminRole,
      {headers: headers});
  }

  getAdminRoles() {
    console.log('Read');
    return this.http.get(AppConstants.API_ENDPOINT + 'adminRole/findAll')
      .map(
        (response: Response) => {
          return response.json();
        }
      )
      .catch(
        (error: Response) => {
          return Observable.throw('Something went wrong' + error);
        }
      );
  }

  getById(objId: string) {
    return this.http.get(AppConstants.API_ENDPOINT + 'adminRole/findById/' + objId)
      .map(
        (response: Response) => {
          return response.json();
        }
      )
      .catch(
        (error: Response) => {
          return Observable.throw('Something went wrong' + error);
        }
      );
  }

  getByAdmin(id: string) {
    return this.http.get(AppConstants.API_ENDPOINT + 'adminRole/findByAdmin/' + id)
      .map(
        (response: Response) => {
          return response.json();
        }
      )
      .catch(
        (error: Response) => {
          return Observable.throw('Something went wrong' + error);
        }
      );
  }

  getDataByAdmin(id: string) {
    return this.http.get(AppConstants.API_ENDPOINT + 'adminRole/findDataByAdmin/' + id)
      .map(
        (response: Response) => {
          return response.json();
        }
      )
      .catch(
        (error: Response) => {
          return Observable.throw('Something went wrong' + error);
        }
      );
  }

  updateAdminRole(adminRole: any) {
    console.log('Update');
    const headers = new Headers({'Content-Type': 'application/json'});
    return this.http.put(AppConstants.API_ENDPOINT + 'adminRole/update',
      adminRole,
      {headers: headers});
  }

  deleteAdminRole(obj: any) {
    console.log('Delete');
    const headers = new Headers({'Content-Type': 'application/json'});
    return this.http.delete(AppConstants.API_ENDPOINT + 'adminRole/delete',
      new RequestOptions({
        headers: headers,
        body: obj
      }));
  }

}
