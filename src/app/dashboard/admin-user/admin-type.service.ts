import {Injectable, Input} from '@angular/core';
import {Headers, Http, RequestOptions, Response} from '@angular/http';
import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';
import {AppConstants} from '../../app-constants';

@Injectable()
export class AdminTypeService {

  @Input() selectedObject: any;

  constructor(private http: Http) {
  }

  createAdminType(adminType: any) {
    console.log('Create');
    const headers = new Headers({'Content-Type': 'application/json'});

    return this.http.post(AppConstants.API_ENDPOINT + 'adminType/create',
      adminType,
      {headers: headers});
  }

  getAdminTypes() {
    console.log('Read');
    return this.http.get(AppConstants.API_ENDPOINT + 'adminType/findAll')
      .map(
        (response: Response) => {
          return response.json();
        }
      )
      .catch(
        (error: Response) => {
          return Observable.throw('Something went wrong' + error);
        }
      );
  }

  getById(objId: string) {
    return this.http.get(AppConstants.API_ENDPOINT + 'adminType/findById/' + objId)
      .map(
        (response: Response) => {
          const data = response.json();
          return data.content;
        }
      )
      .catch(
        (error: Response) => {
          return Observable.throw('Something went wrong' + error);
        }
      );
  }

  updateAdminType(adminType: any) {
    console.log('Update');
    const headers = new Headers({'Content-Type': 'application/json'});
    return this.http.put(AppConstants.API_ENDPOINT + 'adminType/update',
      adminType,
      {headers: headers});
  }

  deleteAdminType(obj: any) {
    console.log('Delete');
    const headers = new Headers({'Content-Type': 'application/json'});
    return this.http.delete(AppConstants.API_ENDPOINT + 'adminType/delete',
      new RequestOptions({
        headers: headers,
        body: obj
      }));
  }

}
