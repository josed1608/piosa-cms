import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AdminUserService} from '../../../admin-user.service';

@Component({
  selector: 'app-admin-user-table',
  templateUrl: './admin-user-table.component.html',
  styleUrls: ['./admin-user-table.component.scss']
})
export class AdminUserTableComponent implements OnInit {

    content = [];

    constructor(private router: Router,
                private route: ActivatedRoute,
                private adminUserService: AdminUserService) {
    }

    ngOnInit() {
        this.getData();
    }

    getData() {
        this.adminUserService.getAdminUsers()
            .subscribe(
                (results: any[]) => {
                    this.content = results;

                });
    }

    onClickEdit(object: any) {
        this.adminUserService.selectedObject = object;
        this.router.navigate(['/dashboard/admin-user/edit'], {
            relativeTo: this.route,
            queryParams: {id: object.id},
            // fragment: 'loading'
        });
    }

    onClickErase(object: any) {
        this.adminUserService.deleteAdminUser(object)
            .subscribe(
                (response) => {
                    console.log(response);
                    this.getData();
                },
                (error) => {
                    console.log(error);
                    this.content = [];
                }
            );
    }


    onClickCreate() {
        this.adminUserService.selectedObject = null;
        this.router.navigate(['/dashboard/admin-user/new'], {
            relativeTo: this.route,
        });
    }

}
