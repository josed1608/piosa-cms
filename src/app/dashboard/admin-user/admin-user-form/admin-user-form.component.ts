import {Component, ElementRef, OnInit, ViewContainerRef} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';
import {EntityService} from '../entity.service';
import {RoleTypeService} from '../role-type.service';
import {AdminRoleService} from '../admin-role.service';
import {AdminUserService} from '../../../admin-user.service';
import {ToastsManager} from 'ng2-toastr';
import {AdminTypeService} from '../admin-type.service';

@Component({
    selector: 'app-admin-user-form',
    templateUrl: './admin-user-form.component.html',
    styleUrls: ['./admin-user-form.component.scss']
})
export class AdminUserFormComponent implements OnInit {

    paramsSubscription: Subscription;
    adminUserForm: FormGroup;

    object: any;
    loading = false;
    pageType: string;

    adminRoles = [];
    adminTypes = [];

    addNewRole = false;
    addingRole = false;
    editingRole = false;

    newRole = {};
    entities = [];
    roleTypes = [];

    constructor(public toastr: ToastsManager,
                vRef: ViewContainerRef,
                private router: Router,
                private element: ElementRef,
                private route: ActivatedRoute,
                private adminUserService: AdminUserService,
                private adminTypeService: AdminTypeService,
                private adminRoleService: AdminRoleService,
                private roleTypeService: RoleTypeService,
                private entityService: EntityService,
                private formBuilder: FormBuilder) {

        this.toastr.setRootViewContainerRef(vRef);
    }


    loadEntities() {
        this.entityService.getEntities()
            .subscribe(
                (results: any[]) => {
                    this.entities = results;
                    // if (this.entities.length > 0) {
                    //   // this.newRole['entity_id'] = this.entities[0];
                    //   this.currentEntity = this.entities[0];
                    // }
                });
    }

    loadAdminTypes() {
        this.adminTypeService.getAdminTypes()
            .subscribe(
                (results: any[]) => {
                    this.adminTypes = results;
                    if (this.entities.length > 0) {
                      this.newRole['entity_id'] = this.entities[0];
                    }
                });
    }

    loadAdminRoles() {
        console.log(this.object);
        if (this.object != null) {
            this.adminRoleService.getDataByAdmin(this.object.id)
                .subscribe(
                    (results: any[]) => {
                        console.log(results);
                        this.adminRoles = results;
                    },
                    (error) => {
                        this.adminRoles = [];
                    });
        }
    }

    loadRoleTypes() {
        this.roleTypeService.getRoleTypes()
            .subscribe(
                (results: any[]) => {
                    this.roleTypes = results;
                    if (this.roleTypes.length > 0) {
                      this.newRole['role_type'] = this.roleTypes[0];
                    }

                });
    }

    ngOnInit() {
        this.adminUserForm = this.formBuilder.group({
            name: ['', [Validators.required]],
            email: ['', Validators.required],
            active: ['', Validators.nullValidator],
        });


        this.loadEntities();
        this.loadRoleTypes();
        this.loadAdminTypes();

        this.object = this.adminUserService.selectedObject;
        if (this.object != null) {
            this.loadFromObject();
        }

        this.route.queryParams
            .subscribe(
                (queryParams: Params) => {
                    const id = queryParams['id'];
                    console.log(id);
                    if (id != null) {
                        this.adminUserService.getById(id).subscribe(
                            (data: any) => {
                                if (data != null && data.length > 0) {
                                    this.object = data[0];
                                    console.log(this.object);
                                    this.loadFromObject();
                                }
                            });
                    }
                });

        this.paramsSubscription = this.route.params
            .subscribe(
                (params: Params) => {
                    this.pageType = params['id'];
                }
            );


    }

    loadFromObject() {

        if (this.object.name != null) {
            this.adminUserForm.patchValue({
                'name': this.object.name
            });
        }

        if (this.object.email != null) {
            this.adminUserForm.patchValue({
                'email': this.object.email
            });
        }

        if (this.object.active != null) {
            this.adminUserForm.patchValue({
                'active': this.object.active
            });
        }

        this.loadAdminRoles();

    }

    onCreate() {
        const object = this.adminUserForm.value;
        console.log(object);
        this.adminUserService.createAdminUser(object)
            .subscribe(
                (response) => {
                    console.log(response);
                    this.object = response;

                    this.router.navigate(['../'], {
                        relativeTo: this.route,
                    });
                },
                (error) => {
                    this.toastr.error('Ha ocurrido un error!', 'Ocurrió un error!');
                    console.log(error);
                }
            );
    }


    onUpdate() {
        // console.log(this.articleForm.value);
        const object = this.adminUserForm.value;
        object.id = this.object.id;
        console.log(object);
        this.adminUserService.updateAdminUser(object)
            .subscribe(
                (response) => {
                    console.log(response);
                    this.router.navigate(['../'], {
                        relativeTo: this.route,
                        queryParams: {success: true},
                    });
                },
                (error) => {
                    this.toastr.error('Ha ocurrido un error!', 'Ocurrió un error!');
                    console.log(error);
                }
            );
    }

    onAddRoles() {
        this.addNewRole = true;
        this.addingRole = true;
        this.newRole = {};
        if (this.entities.length > 0) {
            this.newRole['entity_id'] = this.entities[0];
        }
        if (this.roleTypes.length > 0) {
            this.newRole['role_type'] = this.roleTypes[0];
        }
    }

    onCancelRoles() {
        this.editingRole = false;
        this.addNewRole = false;
        this.addingRole = false;
    }

    onClickAddRole() {
        const entity = this.newRole['entity_id'];
        const roleType = this.newRole['role_type'];
        const roleObject = {
            'admin_id': this.object.id,
            'entity_id': entity,
            'role_type': roleType
        };

        console.log(this.newRole);
        console.log(roleObject);
        this.adminRoleService.createAdminRole(roleObject)
            .subscribe(
                (response) => {
                    console.log(response);
                    this.onAddRoles();
                    this.loadAdminRoles();
                },
                (error) => {

                    const responseStatus = error.status;
                    console.log('resp' + responseStatus);
                    if (responseStatus == 410) {
                        this.toastr.error('El usuario ya tiene este rol', 'Ocurrió un error!',
                            {positionClass: 'toast-bottom-right'});
                        console.log(error);
                    } else if (responseStatus == 411) {

                    } else if (responseStatus == 412) {

                    } else if (responseStatus == 413) {

                    }
                }
            );
    }

    onClickUpdateRole() {
        const entity = this.newRole['entity_id'];
        const roleType = this.newRole['role_type'];
        const roleObject = {
            'admin_id': this.object.id,
            'entity_id': entity,
            'role_type': roleType
        };

        console.log(this.newRole);
        console.log(roleObject);
        this.adminRoleService.updateAdminRole(roleObject)
            .subscribe(
                (response) => {
                    console.log(response);
                    this.onAddRoles();
                    this.loadAdminRoles();
                },
                (error) => {

                    const responseStatus = error.status;
                    console.log('resp' + responseStatus);
                    if (responseStatus == 410) {
                        this.toastr.error('El usuario ya tiene este rol', 'Ocurrió un error!',
                            {positionClass: 'toast-bottom-right'});
                        console.log(error);
                    } else if (responseStatus == 411) {

                    } else if (responseStatus == 412) {

                    } else if (responseStatus == 413) {

                    }
                }
            );
    }

    onEditRole(object: any) {
        console.log(object);
        this.newRole['entity_id'] = object.entity_id;
        this.newRole['role_type'] = object.role_type;
        this.editingRole = true;
        this.addNewRole = true;
        this.addingRole = true;

    }


    onDeleteRole(object: any) {
        object['admin_id'] = this.object.id;
        this.adminRoleService.deleteAdminRole(object)
            .subscribe(
                (response) => {
                    console.log(response);
                    this.loadAdminRoles();
                },
                (error) => {
                    console.log(error);
                }
            );
    }

}
