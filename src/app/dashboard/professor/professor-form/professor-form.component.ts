import {Component, ElementRef, OnInit, ViewContainerRef} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToastsManager} from 'ng2-toastr';
import {Subscription} from 'rxjs/Subscription';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {ProfessorService} from '../professor.service';
import * as AWS from 'aws-sdk';

@Component({
    selector: 'app-professor-form',
    templateUrl: './professor-form.component.html',
    styleUrls: ['./professor-form.component.scss']
})
export class ProfessorFormComponent implements OnInit {

    paramsSubscription: Subscription;
    professorForm: FormGroup;
    s3: any;
    object: any;
    loading = false;
    pageType: string;

    creation_date: any;
    modif_date: any;

    constructor(public toastr: ToastsManager,
                vRef: ViewContainerRef,
                private router: Router,
                private element: ElementRef,
                private route: ActivatedRoute,
                private professorService: ProfessorService,
                private formBuilder: FormBuilder) {

        this.toastr.setRootViewContainerRef(vRef);

        this.s3 = new AWS.S3({
            params: {
                Bucket: 'piosa-storage-bucket'
            }
        });

        const creds = new AWS.Credentials('AKIAIJ4PSDOZABQ3OTPQ', 'vfeGuLB4izhClVcsgXJXWkqHMLwlyzHhEe1b7pf5', '');
        this.s3.config.update({
            region: 'us-west-2',
            credentials: creds
        });
    }

    ngOnInit() {

        this.professorForm = this.formBuilder.group({
            name: ['', [Validators.required]],
            last_name: ['', [Validators.nullValidator]],
            sur_name: ['', [Validators.nullValidator]],
            img_url: ['', Validators.nullValidator],
            active: ['', Validators.nullValidator],
        });

        this.object = this.professorService.selectedObject;
        if (this.object != null) {
            this.loadFromObject();
        }

        this.route.queryParams
            .subscribe(
                (queryParams: Params) => {
                    const id = queryParams['id'];
                    console.log(id);
                    if (id != null) {
                        this.professorService.getById(id).subscribe(
                            (data: any) => {
                                if (data != null && data.length > 0) {
                                    this.object = data[0];
                                    console.log(this.object);
                                    this.loadFromObject();
                                }
                            });
                    }
                });

        this.paramsSubscription = this.route.params
            .subscribe(
                (params: Params) => {
                    this.pageType = params['id'];
                }
            );
    }

    loadFromObject() {

        if (this.object.name != null) {
            this.professorForm.patchValue({
                'name': this.object.name
            });
        }

        if (this.object.last_name != null) {
            this.professorForm.patchValue({
                'last_name': this.object.last_name
            });
        }

        if (this.object.sur_name != null) {
            this.professorForm.patchValue({
                'sur_name': this.object.sur_name
            });
        }

        if (this.object.active != null) {
            this.professorForm.patchValue({
                'active': this.object.active
            });
        }

        if (this.object.creation_date != null) {
            this.creation_date = new Date(this.object.creation_date);
        }

        if (this.object.modif_date != null) {
            this.modif_date = new Date(this.object.modif_date);
        }

        if (this.object.img_url != null) {
            this.professorForm.patchValue({
                'img_url': this.object.img_url
            });

            const mImage = this.element.nativeElement.querySelector('#profImg');
            mImage.src = this.object.img_url;
            mImage.hidden = false;

        } else {

            const mImage = this.element.nativeElement.querySelector('#profImg');
            mImage.hidden = true;
        }
    }

    onCreate() {
        const object = this.professorForm.value;
        console.log(object);
        this.professorService.createProfessor(object)
            .subscribe(
                (response) => {
                    console.log(response);
                    this.object = response;

                    this.router.navigate(['../'], {
                        relativeTo: this.route,
                    });
                },
                (error) => {
                    this.toastr.error('Ha ocurrido un error!', 'Ocurrió un error!');
                    console.log(error);
                }
            );
    }


    onUpdate() {
        const object = this.professorForm.value;
        object.id = this.object.id;
        console.log(object);
        this.professorService.updateProfessor(object)
            .subscribe(
                (response) => {
                    console.log(response);
                    this.router.navigate(['../'], {
                        relativeTo: this.route,
                        queryParams: {success: true},
                    });
                },
                (error) => {
                    this.toastr.error('Ha ocurrido un error!', 'Ocurrió un error!');
                    console.log(error);
                }
            );
    }


    onImageChange(event) {
        const file = event.target.files[0];
        const res = this.uploadFile(file);
        console.log('El resultado es ' + res);
    }

    uploadFile(file: any) {
        console.log(file);

        this.s3.upload({
            Key: file.name,
            Body: file,
            ContentType: file.type,
            ACL: 'public-read'

        }, (err: any, data: any) => {
            if (err) {
                console.log(err);
                // alert(err.message);
            } else {
                console.log(data);
                // alert('Imagen subida');
                this.professorForm.patchValue({
                    'img_url': data.Location
                });

                const image = this.element.nativeElement.querySelector('#profImg');
                image.src = data.Location;
                image.hidden = false;

            }
        });
    }
}
