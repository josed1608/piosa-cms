import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ProfessorService} from '../professor.service';

@Component({
    selector: 'app-professor-table',
    templateUrl: './professor-table.component.html',
    styleUrls: ['./professor-table.component.scss']
})
export class ProfessorTableComponent implements OnInit {

    content = [];

    constructor(private router: Router,
                private route: ActivatedRoute,
                private professorService: ProfessorService) {
    }

    ngOnInit() {
        this.getData();
    }

    getData() {
        this.professorService.getProfessors()
            .subscribe(
                (results: any[]) => {
                    this.content = results;

                });
    }

    onClickEdit(object: any) {
        this.professorService.selectedObject = object;
        this.router.navigate(['/dashboard/professor/edit'], {
            relativeTo: this.route,
            queryParams: {id: object.id},
            // fragment: 'loading'
        });
    }

    onClickErase(object: any) {
        this.professorService.deleteProfessor(object)
            .subscribe(
                (response) => {
                    console.log(response);
                    this.getData();
                },
                (error) => {
                    console.log(error);
                }
            );
    }


    onClickCreate() {
        this.professorService.selectedObject = null;
        this.router.navigate(['/dashboard/professor/new'], {
            relativeTo: this.route,
        });
    }


    getFullName(obj: any) {
        if (obj != null) {
            let str = '';

            if (obj.name != null) {
                str += obj.name + ' ';
            }

            if (obj.last_name != null) {
                str += obj.last_name + ' ';
            }

            if (obj.sur_name != null) {
                str += obj.sur_name;

            }
            return str;
        }
    }
}
