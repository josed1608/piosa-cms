import {Injectable, Input} from '@angular/core';
import {Headers, Http, RequestOptions, Response} from '@angular/http';
import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';
import {AppConstants} from '../../app-constants';

@Injectable()
export class ProfessorService {

    @Input() selectedObject: any;

    constructor(private http: Http) {
    }

    createProfessor(professor: any) {
        console.log('Create');
        const headers = new Headers({'Content-Type': 'application/json'});

        return this.http.post(AppConstants.API_ENDPOINT + 'professor/create',
            professor,
            {headers: headers});
    }

    getProfessors() {
        console.log('Read');
        return this.http.get(AppConstants.API_ENDPOINT + 'professor/findAll')
            .map(
                (response: Response) => {
                    return response.json();
                }
            )
            .catch(
                (error: Response) => {
                    return Observable.throw('Something went wrong' + error);
                }
            );
    }

    getById(objId: string) {
        return this.http.get(AppConstants.API_ENDPOINT + 'professor/findById/' + objId)
            .map(
                (response: Response) => {
                    return response.json();
                }
            )
            .catch(
                (error: Response) => {
                    return Observable.throw('Something went wrong' + error);
                }
            );
    }

    updateProfessor(professor: any) {
        console.log('Update');
        const headers = new Headers({'Content-Type': 'application/json'});
        return this.http.put(AppConstants.API_ENDPOINT + 'professor/update',
            professor,
            {headers: headers});
    }

    deleteProfessor(obj: any) {
        console.log('Delete');
        const headers = new Headers({'Content-Type': 'application/json'});
        return this.http.delete(AppConstants.API_ENDPOINT + 'professor/delete',
            new RequestOptions({
                headers: headers,
                body: obj
            }));
    }

}
