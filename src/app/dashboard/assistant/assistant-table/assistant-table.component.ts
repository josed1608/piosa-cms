import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AssistantService} from '../assistant.service';

@Component({
    selector: 'app-assistant-table',
    templateUrl: './assistant-table.component.html',
    styleUrls: ['./assistant-table.component.scss']
})
export class AssistantTableComponent implements OnInit {

    content = [];

    constructor(private router: Router,
                private route: ActivatedRoute,
                private assistantService: AssistantService) {
    }

    ngOnInit() {
        this.getData();
    }

    getData() {
        this.assistantService.getAssistants()
            .subscribe(
                (results: any[]) => {
                    this.content = results;

                });
    }

    onClickEdit(object: any) {
        this.assistantService.selectedObject = object;
        this.router.navigate(['/dashboard/assistant/edit'], {
            relativeTo: this.route,
            queryParams: {id: object.id},
            // fragment: 'loading'
        });
    }

    onClickErase(object: any) {
        this.assistantService.deleteAssistant(object)
            .subscribe(
                (response) => {
                    console.log(response);
                    this.getData();
                },
                (error) => {
                    console.log(error);
                }
            );
    }


    onClickCreate() {
        this.assistantService.selectedObject = null;
        this.router.navigate(['/dashboard/assistant/new'], {
            relativeTo: this.route,
        });
    }

    getFullName(obj: any) {
        if (obj != null) {
            let str = '';

            if (obj.name != null) {
                str += obj.name + ' ';
            }

            if (obj.last_name != null) {
                str += obj.last_name + ' ';
            }

            if (obj.sur_name != null) {
                str += obj.sur_name;

            }
            return str;
        }
    }
}
