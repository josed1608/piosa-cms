import {Injectable, Input} from '@angular/core';
import {Headers, Http, RequestOptions, Response} from '@angular/http';
import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';
import {AppConstants} from '../../app-constants';

@Injectable()
export class WorkTeamAssistantService {

    @Input() selectedObject: any;

    constructor(private http: Http) {
    }

    createWorkTeamAssistant(workTeamAssistant: any) {
        console.log('Create');
        const headers = new Headers({'Content-Type': 'application/json'});

        return this.http.post(AppConstants.API_ENDPOINT + 'workTeamAssistant/create',
            workTeamAssistant,
            {headers: headers});
    }

    getWorkTeamAssistants(team: number) {
        console.log('Read');
        return this.http.get(AppConstants.API_ENDPOINT + 'workTeamAssistant/findDataByTeam/' + team)
            .map(
                (response: Response) => {
                    return response.json();
                }
            )
            .catch(
                (error: Response) => {
                    return Observable.throw('Something went wrong' + error);
                }
            );
    }

    getById(objId: string) {
        return this.http.get(AppConstants.API_ENDPOINT + 'workTeamAssistant/findById/' + objId)
            .map(
                (response: Response) => {
                    return response.json();
                }
            )
            .catch(
                (error: Response) => {
                    return Observable.throw('Something went wrong' + error);
                }
            );
    }


    deleteWorkTeamAssistant(obj: any) {
        console.log('Delete');
        const headers = new Headers({'Content-Type': 'application/json'});
        return this.http.delete(AppConstants.API_ENDPOINT + 'workTeamAssistant/delete',
            new RequestOptions({
                headers: headers,
                body: obj
            }));
    }

}
