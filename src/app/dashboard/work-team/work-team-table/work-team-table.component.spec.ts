import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkTeamTableComponent } from './work-team-table.component';

describe('WorkTeamTableComponent', () => {
  let component: WorkTeamTableComponent;
  let fixture: ComponentFixture<WorkTeamTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkTeamTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkTeamTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
