import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {WorkTeamService} from '../work-team.service';

@Component({
    selector: 'app-work-team-table',
    templateUrl: './work-team-table.component.html',
    styleUrls: ['./work-team-table.component.scss']
})
export class WorkTeamTableComponent implements OnInit {

    content = [];

    constructor(private router: Router,
                private route: ActivatedRoute,
                private workTeamService: WorkTeamService) {
    }

    ngOnInit() {
        this.getData();
    }

    getData() {
        this.workTeamService.getWorkTeams()
            .subscribe(
                (results: any[]) => {
                    this.content = results;

                }, (error1) => {
                    this.content = [];
                }
            );
    }

    onClickEdit(object: any) {
        this.workTeamService.selectedObject = object;
        this.router.navigate(['/dashboard/work-team/edit'], {
            relativeTo: this.route,
            queryParams: {id: object.id},
            // fragment: 'loading'
        });
    }

    onClickErase(object: any) {
        this.workTeamService.deleteWorkTeam(object)
            .subscribe(
                (response) => {
                    console.log(response);
                    this.getData();
                },
                (error) => {
                    this.content = [];
                    console.log(error);
                }
            );
    }


    onClickCreate() {
        this.workTeamService.selectedObject = null;
        this.router.navigate(['/dashboard/work-team/new'], {
            relativeTo: this.route,
        });
    }

}
