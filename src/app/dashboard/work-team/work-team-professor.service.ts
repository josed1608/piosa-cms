import {Injectable, Input} from '@angular/core';
import {Headers, Http, RequestOptions, Response} from '@angular/http';
import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';
import {AppConstants} from '../../app-constants';

@Injectable()
export class WorkTeamProfessorService {

    @Input() selectedObject: any;

    constructor(private http: Http) {
    }

    createWorkTeamProfessor(workTeamProfessor: any) {
        console.log('Create');
        const headers = new Headers({'Content-Type': 'application/json'});

        return this.http.post(AppConstants.API_ENDPOINT + 'workTeamProfessor/create',
            workTeamProfessor,
            {headers: headers});
    }

    getWorkTeamProfessors(team: number) {
        console.log('Read');
        return this.http.get(AppConstants.API_ENDPOINT + 'workTeamProfessor/findDataByTeam/' + team)
            .map(
                (response: Response) => {
                    return response.json();
                }
            )
            .catch(
                (error: Response) => {
                    return Observable.throw('Something went wrong' + error);
                }
            );
    }

    getById(objId: string) {
        return this.http.get(AppConstants.API_ENDPOINT + 'workTeamProfessor/findById/' + objId)
            .map(
                (response: Response) => {
                    return response.json();
                }
            )
            .catch(
                (error: Response) => {
                    return Observable.throw('Something went wrong' + error);
                }
            );
    }


    deleteWorkTeamProfessor(obj: any) {
        console.log('Delete');
        const headers = new Headers({'Content-Type': 'application/json'});
        return this.http.delete(AppConstants.API_ENDPOINT + 'workTeamProfessor/delete',
            new RequestOptions({
                headers: headers,
                body: obj
            }));
    }

}
