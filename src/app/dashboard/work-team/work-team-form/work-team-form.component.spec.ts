import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkTeamFormComponent } from './work-team-form.component';

describe('WorkTeamFormComponent', () => {
  let component: WorkTeamFormComponent;
  let fixture: ComponentFixture<WorkTeamFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkTeamFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkTeamFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
