import {Component, ElementRef, OnInit, ViewContainerRef} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';
import {ToastsManager} from 'ng2-toastr';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ProfessorService} from '../../professor/professor.service';
import {WorkTeamService} from '../work-team.service';
import {WorkTeamAssistantService} from '../work-team-assistant.service';
import {WorkTeamProfessorService} from '../work-team-professor.service';
import {AssistantService} from '../../assistant/assistant.service';

@Component({
    selector: 'app-work-team-form',
    templateUrl: './work-team-form.component.html',
    styleUrls: ['./work-team-form.component.scss']
})
export class WorkTeamFormComponent implements OnInit {

    paramsSubscription: Subscription;
    workTeamForm: FormGroup;

    currCoordinator: any;
    currProfessor: any;
    currAssistant: any;

    professors = [];
    assistants = [];

    workTeamProfessors = [];
    workTeamAssistants = [];

    addingProfessor = false;
    addingAssistant = false;

    object: any;
    loading = false;
    pageType: string;

    creation_date: any;
    modif_date: any;

    constructor(public toastr: ToastsManager,
                vRef: ViewContainerRef,
                private router: Router,
                private element: ElementRef,
                private route: ActivatedRoute,
                private workTeamAssistantService: WorkTeamAssistantService,
                private workTeamProfessorService: WorkTeamProfessorService,
                private assistantService: AssistantService,
                private professorService: ProfessorService,
                private workTeamService: WorkTeamService,
                private formBuilder: FormBuilder) {

        this.toastr.setRootViewContainerRef(vRef);
    }

    ngOnInit() {

        this.workTeamForm = this.formBuilder.group({
            name: ['', [Validators.required]],
            description: ['', Validators.required],
            coordinator_id: ['', Validators.required],
            active: ['', Validators.nullValidator],
        });


        this.loadProfessors();
        this.loadAssistants();

        this.object = this.workTeamService.selectedObject;
        if (this.object != null) {
            this.loadFromObject();
        }

        this.route.queryParams
            .subscribe(
                (queryParams: Params) => {
                    const id = queryParams['id'];
                    console.log(id);
                    if (id != null) {
                        this.workTeamService.getById(id).subscribe(
                            (data: any) => {
                                if (data != null && data.length > 0) {
                                    this.object = data[0];
                                    console.log(this.object);
                                    this.loadFromObject();
                                }
                            });
                    }
                });

        this.paramsSubscription = this.route.params
            .subscribe(
                (params: Params) => {
                    this.pageType = params['id'];
                }
            );


    }


    loadProfessors() {
        this.professorService.getProfessors()
            .subscribe(
                (results: any[]) => {
                    this.professors = results;
                    if (this.professors.length > 0) {
                        if (this.currCoordinator == null) {
                            this.currCoordinator = this.professors[0].id;
                        }
                        if (this.currProfessor == null) {
                            this.currProfessor = this.professors[0].id;
                        }
                    }

                });
    }

    loadAssistants() {
        this.assistantService.getAssistants()
            .subscribe(
                (results: any[]) => {
                    this.assistants = results;
                    if (this.assistants.length > 0) {
                        if (this.currAssistant == null) {
                            this.currAssistant = this.assistants[0].id;
                        }
                    }

                });
    }

    loadFromObject() {

        this.loadWorkTeamProfessors();
        this.loadWorkTeamAssistants();

        if (this.object.name != null) {
            this.workTeamForm.patchValue({
                'name': this.object.name
            });
        }

        if (this.object.description != null) {
            this.workTeamForm.patchValue({
                'description': this.object.description
            });
        }

        if (this.object.active != null) {
            this.workTeamForm.patchValue({
                'active': this.object.active
            });
        }

        const sdate = new Date(this.object.start_date);
        if (this.object.start_date != null) {
            this.workTeamForm.controls['start_date'].setValue(sdate.toISOString().substring(0, 10));
        }

        const edate = new Date(this.object.end_date);
        if (this.object.end_date != null) {
            this.workTeamForm.controls['end_date'].setValue(edate.toISOString().substring(0, 10));
        }

        if (this.object.creation_date != null) {
            this.creation_date = new Date(this.object.creation_date);
        }

        if (this.object.modif_date != null) {
            this.modif_date = new Date(this.object.modif_date);
        }

        if (this.object.coordinator_id != null) {
            this.workTeamForm.patchValue({
                'coordinator_id': this.object.coordinator_id
            });
            this.currCoordinator = this.object.coordinator_id;
        }

    }

    onCreate() {
        const object = this.workTeamForm.value;
        console.log(object);
        this.workTeamService.createWorkTeam(object)
            .subscribe(
                (response) => {
                    console.log(response);
                    this.object = response;

                    this.router.navigate(['../'], {
                        relativeTo: this.route,
                    });
                },
                (error) => {
                    this.toastr.error('Ha ocurrido un error!', 'Ocurrió un error!');
                    console.log(error);
                }
            );
    }


    onUpdate() {
        // console.log(this.articleForm.value);
        const object = this.workTeamForm.value;
        object.id = this.object.id;
        console.log(object);
        this.workTeamService.updateWorkTeam(object)
            .subscribe(
                (response) => {
                    console.log(response);
                    this.router.navigate(['../'], {
                        relativeTo: this.route,
                        queryParams: {success: true},
                    });
                },
                (error) => {
                    this.toastr.error('Ha ocurrido un error!', 'Ocurrió un error!');
                    console.log(error);
                }
            );
    }


    getFullName(obj: any) {
        if (obj != null) {
            let str = '';

            if (obj.name != null) {
                str += obj.name + ' ';
            }

            if (obj.last_name != null) {
                str += obj.last_name + ' ';
            }

            if (obj.sur_name != null) {
                str += obj.sur_name;

            }
            return str;
        }
    }

    onAddProfessor() {
        this.addingProfessor = true;
    }


    onCancelAddProfessor() {
        this.addingProfessor = false;
    }

    onClickAddProfessor() {
        const obj = {professor_id: this.currProfessor, team_id: this.object.id};
        this.workTeamProfessorService.createWorkTeamProfessor(obj)
            .subscribe(
                (response) => {
                    console.log(response);

                    this.loadWorkTeamProfessors();

                },
                (error) => {
                    this.toastr.error('Ha ocurrido un error!', 'Ocurrió un error!');
                    console.log(error);
                }
            );


    }


    loadWorkTeamProfessors() {
        this.workTeamProfessorService.getWorkTeamProfessors(this.object.id)
            .subscribe(
                (results: any[]) => {
                    this.workTeamProfessors = results;
                }, (error1) => {
                    this.workTeamProfessors = [];
                });
    }

    loadWorkTeamAssistants() {
        this.workTeamAssistantService.getWorkTeamAssistants(this.object.id)
            .subscribe(
                (results: any[]) => {
                    this.workTeamAssistants = results;
                }, (error) => {
                    this.workTeamAssistants = [];
                });
    }

    onAddAssistant() {
        this.addingAssistant = true;
    }


    onCancelAddAssistant() {
        this.addingAssistant = false;
    }

    onClickAddAssistant() {
        const obj = {assistant_id: this.currAssistant, team_id: this.object.id};
        this.workTeamAssistantService.createWorkTeamAssistant(obj)
            .subscribe(
                (response) => {
                    console.log(response);
                    this.loadWorkTeamAssistants();

                },
                (error) => {
                    this.toastr.error('Ha ocurrido un error!', 'Ocurrió un error!');
                    console.log(error);
                }
            );
    }

    onDeleteWorkTeamProfessor(obj: any) {
        this.workTeamProfessorService.deleteWorkTeamProfessor(obj)
            .subscribe(
                (response) => {
                    console.log(response);
                    this.loadWorkTeamProfessors();
                },
                (error) => {
                    console.log(error);
                }
            );
    }

    onDeleteWorkTeamAssistant(obj: any) {
        this.workTeamAssistantService.deleteWorkTeamAssistant(obj)
            .subscribe(
                (response) => {
                    console.log(response);
                    this.loadWorkTeamAssistants();
                },
                (error) => {
                    console.log(error);
                }
            );
    }

}
