import {Component, Input, OnInit} from '@angular/core';
import {NewsService} from '../news/news.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-timeline',
    templateUrl: './timeline.component.html',
    styleUrls: ['./timeline.component.scss']
})
export class TimelineComponent implements OnInit {

    @Input() content: any[];

    constructor(private router: Router,
                private route: ActivatedRoute,
                private newsService: NewsService) {
    }

    ngOnInit() {
    }

    getClass(index: number) {
        return (index % 2) ? 'inverted' : '';
    }

    onClickNewsFeed(object: any) {

        this.newsService.selectedObject = {
            content: object.message,
            facebook_id: object.id,
            facebook_link: object.link,
            full_picture: object.full_picture,
        };

        this.router.navigate(['../news/new'], {
            relativeTo: this.route,
            queryParams: {edit: '1', fb_id: object.id}
        });
    }
}
