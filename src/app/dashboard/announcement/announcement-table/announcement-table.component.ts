import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AnnouncementService} from '../announcement.service';

@Component({
    selector: 'app-announcement-table',
    templateUrl: './announcement-table.component.html',
    styleUrls: ['./announcement-table.component.scss']
})
export class AnnouncementTableComponent implements OnInit {

    content = [];

    constructor(private router: Router,
                private route: ActivatedRoute,
                private announcementService: AnnouncementService) {
    }

    ngOnInit() {
        this.getData();
    }

    getData() {
        this.announcementService.getAnnouncements()
            .subscribe(
                (results: any[]) => {
                    this.content = results;

                });
    }

    onClickEdit(object: any) {
        this.announcementService.selectedObject = object;
        this.router.navigate(['/dashboard/announcement/edit'], {
            relativeTo: this.route,
            queryParams: {id: object.id},
            // fragment: 'loading'
        });
    }

    onClickErase(object: any) {
        this.announcementService.deleteAnnouncement(object)
            .subscribe(
                (response) => {
                    console.log(response);
                    this.getData();
                },
                (error) => {
                    console.log(error);
                    this.content = [];
                }
            );
    }


    onClickCreate() {
        this.announcementService.selectedObject = null;
        this.router.navigate(['/dashboard/announcement/new'], {
            relativeTo: this.route,
        });
    }

}
