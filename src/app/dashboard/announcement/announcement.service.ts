import {Injectable, Input} from '@angular/core';
import {Headers, Http, RequestOptions, Response} from '@angular/http';
import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';
import {AppConstants} from '../../app-constants';

@Injectable()
export class AnnouncementService {

    @Input() selectedObject: any;

    constructor(private http: Http) {
    }

    createAnnouncement(announcement: any) {
        console.log('Create');
        const headers = new Headers({'Content-Type': 'application/json'});

        return this.http.post(AppConstants.API_ENDPOINT + 'announcement/create',
            announcement,
            {headers: headers});
    }

    getAnnouncements() {
        console.log('Read');
        return this.http.get(AppConstants.API_ENDPOINT + 'announcement/findAll')
            .map(
                (response: Response) => {
                    return response.json();
                }
            )
            .catch(
                (error: Response) => {
                    return Observable.throw('Something went wrong' + error);
                }
            );
    }

    getById(objId: string) {
        return this.http.get(AppConstants.API_ENDPOINT + 'announcement/findById/' + objId)
            .map(
                (response: Response) => {
                    return response.json();
                }
            )
            .catch(
                (error: Response) => {
                    return Observable.throw('Something went wrong' + error);
                }
            );
    }

    updateAnnouncement(announcement: any) {
        console.log('Update');
        const headers = new Headers({'Content-Type': 'application/json'});
        return this.http.put(AppConstants.API_ENDPOINT + 'announcement/update',
            announcement,
            {headers: headers});
    }

    deleteAnnouncement(obj: any) {
        console.log('Delete');
        const headers = new Headers({'Content-Type': 'application/json'});
        return this.http.delete(AppConstants.API_ENDPOINT + 'announcement/delete',
            new RequestOptions({
                headers: headers,
                body: obj
            }));
    }

}
