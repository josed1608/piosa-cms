import {Component, OnInit} from '@angular/core';

declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
}

export const ROUTES: RouteInfo[] = [
    {path: 'home', title: 'Inicio', icon: 'home'},
    {path: 'admin-user', title: 'Usuarios Admin', icon: 'tag'},
    // {path: 'user', title: 'Usuarios', icon: 'hashtag'},
    {path: 'proyect', title: 'Proyectos', icon: 'futbol-o'},
    {path: 'advance', title: 'Avances', icon: 'futbol-o'},
    {path: 'announcement', title: 'Anuncios', icon: 'futbol-o'},
    {path: 'event', title: 'Eventos', icon: 'flag'},
    {path: 'news', title: 'Noticias', icon: 'futbol-o'},
    {path: 'professor', title: 'Profesores', icon: 'futbol-o'},
    {path: 'assistant', title: 'Asistentes', icon: 'flag'},
    {path: 'work-team', title: 'Equipo de Trabajo', icon: 'futbol-o'},
    {path: 'social-actor', title: 'Actores Sociales', icon: 'futbol-o'},
];

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

    menuItems: any[];
    isActive = false;
    showMenu = '';
    eventCalled() {
        this.isActive = !this.isActive;
    }
    addExpandClass(element: any) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        } else {
            this.showMenu = element;
        }
    }
    constructor() {
    }

    ngOnInit() {
        this.menuItems = ROUTES.filter(menuItem => menuItem);
    }


}
