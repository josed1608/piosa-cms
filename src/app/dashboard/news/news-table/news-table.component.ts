import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {NewsService} from '../news.service';

@Component({
    selector: 'app-news-table',
    templateUrl: './news-table.component.html',
    styleUrls: ['./news-table.component.scss']
})
export class NewsTableComponent implements OnInit {

    content = [];

    constructor(private router: Router,
                private route: ActivatedRoute,
                private newsService: NewsService) {
    }

    ngOnInit() {
        this.getData();
    }

    getData() {
        this.newsService.getNews()
            .subscribe(
                (results: any[]) => {
                    this.content = results;

                });
    }

    onClickEdit(object: any) {
        this.newsService.selectedObject = object;
        this.router.navigate(['/dashboard/news/edit'], {
            relativeTo: this.route,
            queryParams: {id: object.id},
            // fragment: 'loading'
        });
    }

    onClickErase(object: any) {
        this.newsService.deleteNews(object)
            .subscribe(
                (response) => {
                    console.log(response);
                    this.getData();
                },
                (error) => {
                    console.log(error);
                }
            );
    }


    onClickCreate() {
        this.newsService.selectedObject = null;
        this.router.navigate(['/dashboard/news/new'], {
            relativeTo: this.route,
        });
    }

}
