import {Component, ElementRef, OnInit, ViewContainerRef} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ProyectService} from '../../proyect/proyect.service';
import {ToastsManager} from 'ng2-toastr';
import {Subscription} from 'rxjs/Subscription';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {NewsService} from '../news.service';
import * as AWS from 'aws-sdk';

@Component({
    selector: 'app-news-form',
    templateUrl: './news-form.component.html',
    styleUrls: ['./news-form.component.scss']
})
export class NewsFormComponent implements OnInit {

    paramsSubscription: Subscription;
    newsForm: FormGroup;

    currentProy: any;
    proyects = [];
    s3: any;
    object: any;
    loading = false;
    pageType: string;

    creation_date: any;
    modif_date: any;

    constructor(public toastr: ToastsManager,
                vRef: ViewContainerRef,
                private router: Router,
                private element: ElementRef,
                private route: ActivatedRoute,
                private proyectService: ProyectService,
                private newsService: NewsService,
                private formBuilder: FormBuilder) {

        this.toastr.setRootViewContainerRef(vRef);

        this.s3 = new AWS.S3({
            params: {
                Bucket: 'piosa-storage-bucket'
            }
        });

        const creds = new AWS.Credentials('AKIAIJ4PSDOZABQ3OTPQ', 'vfeGuLB4izhClVcsgXJXWkqHMLwlyzHhEe1b7pf5', '');
        this.s3.config.update({
            region: 'us-west-2',
            credentials: creds
        });
    }

    ngOnInit() {

        this.newsForm = this.formBuilder.group({
            title: ['', [Validators.required]],
            content: ['', Validators.required],
            resume: ['', Validators.required],
            proyect_id: ['', Validators.required],
            img_url: ['', Validators.nullValidator],
            link: ['', Validators.nullValidator],
            active: ['', Validators.nullValidator],
        });

        this.loadProyects();
        this.object = this.newsService.selectedObject;
        if (this.object != null) {
            this.loadFromObject();
        }

        this.route.queryParams
            .subscribe(
                (queryParams: Params) => {
                    const id = queryParams['id'];
                    console.log(id);
                    if (id != null) {
                        this.newsService.getById(id).subscribe(
                            (data: any) => {
                                if (data != null && data.length > 0) {
                                    this.object = data[0];
                                    console.log(this.object);
                                    this.loadFromObject();
                                }
                            });
                    }
                });

        this.paramsSubscription = this.route.params
            .subscribe(
                (params: Params) => {
                    this.pageType = params['id'];
                }
            );


    }


    loadProyects() {
        this.proyectService.getProyects()
            .subscribe(
                (results: any[]) => {
                    this.proyects = results;
                    if (this.proyects.length > 0) {
                        if (this.currentProy == null) {
                            this.currentProy = this.proyects[0].id;
                        }
                    }

                });
    }

    loadFromObject() {

        if (this.object.title != null) {
            this.newsForm.patchValue({
                'title': this.object.title
            });
        }

        if (this.object.resume != null) {
            this.newsForm.patchValue({
                'resume': this.object.resume
            });
        }

        if (this.object.content != null) {
            this.newsForm.patchValue({
                'content': this.object.content
            });
        }

        if (this.object.link != null) {
            this.newsForm.patchValue({
                'link': this.object.link
            });
        }

        if (this.object.active != null) {
            this.newsForm.patchValue({
                'active': this.object.active
            });
        }

        const sdate = new Date(this.object.start_date);
        if (this.object.start_date != null) {
            this.newsForm.controls['start_date'].setValue(sdate.toISOString().substring(0, 10));
        }

        const edate = new Date(this.object.end_date);
        if (this.object.end_date != null) {
            this.newsForm.controls['end_date'].setValue(edate.toISOString().substring(0, 10));
        }

        if (this.object.creation_date != null) {
            this.creation_date = new Date(this.object.creation_date);
        }

        if (this.object.modif_date != null) {
            this.modif_date = new Date(this.object.modif_date);
        }

        if (this.object.type != null) {
            this.newsForm.patchValue({
                'type': this.object.type
            });
            this.currentProy = this.object.type;
        }

        if (this.object.img_url != null) {
            this.newsForm.patchValue({
                'img_url': this.object.img_url
            });

            const mImage = this.element.nativeElement.querySelector('#newsImage');
            mImage.src = this.object.img_url;
            mImage.hidden = false;

        } else {

            const mImage = this.element.nativeElement.querySelector('#newsImage');
            mImage.hidden = true;
        }

        if (this.object.full_picture != null) {
            this.newsForm.patchValue({
                'img_url': this.object.full_picture
            });

            const mImage = this.element.nativeElement.querySelector('#newsImage');
            mImage.src = this.object.full_picture;
            mImage.hidden = false;
        }

    }

    onCreate() {
        const object = this.newsForm.value;
        console.log(object);
        this.newsService.createNews(object)
            .subscribe(
                (response) => {
                    console.log(response);
                    this.object = response;

                    this.router.navigate(['../'], {
                        relativeTo: this.route,
                    });
                },
                (error) => {
                    this.toastr.error('Ha ocurrido un error!', 'Ocurrió un error!');
                    console.log(error);
                }
            );
    }


    onUpdate() {
        // console.log(this.articleForm.value);
        const object = this.newsForm.value;
        object.id = this.object.id;
        console.log(object);
        this.newsService.updateNews(object)
            .subscribe(
                (response) => {
                    console.log(response);
                    this.router.navigate(['../'], {
                        relativeTo: this.route,
                        queryParams: {success: true},
                    });
                },
                (error) => {
                    this.toastr.error('Ha ocurrido un error!', 'Ocurrió un error!');
                    console.log(error);
                }
            );
    }

    onImageChange(event) {
        const file = event.target.files[0];
        const res = this.uploadFile(file);
        console.log('El resultado es ' + res);
    }

    uploadFile(file: any) {
        console.log(file);

        this.s3.upload({
            Key: file.name,
            Body: file,
            ContentType: file.type,
            ACL: 'public-read'

        }, (err: any, data: any) => {
            if (err) {
                console.log(err);
                // alert(err.message);
            } else {
                console.log(data);
                // alert('Imagen subida');
                this.newsForm.patchValue({
                    'img_url': data.Location
                });

                const image = this.element.nativeElement.querySelector('#newsImage');
                image.src = data.Location;
                image.hidden = false;

            }
        });
    }

}
