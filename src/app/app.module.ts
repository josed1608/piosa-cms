import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {LoginComponent} from './login/login.component';
import {NotFoundComponent} from './not-found/not-found.component';
import {NgbAlertModule, NgbCarouselModule, NgbDropdownModule} from '@ng-bootstrap/ng-bootstrap';
import {ToastModule} from 'ng2-toastr';
import {AdminUserService} from './admin-user.service';
import {HomeComponent} from './dashboard/home/home.component';
import {AdminUserTableComponent} from './dashboard/admin-user/admin-user-table/admin-user-table.component';
import {AdminUserFormComponent} from './dashboard/admin-user/admin-user-form/admin-user-form.component';
import {UserFormComponent} from './dashboard/user/user-form/user-form.component';
import {UserTableComponent} from './dashboard/user/user-table/user-table.component';
import {EventTableComponent} from './dashboard/event/event-table/event-table.component';
import {EventFormComponent} from './dashboard/event/event-form/event-form.component';
import {AdvanceFormComponent} from './dashboard/advance/advance-form/advance-form.component';
import {AdvanceTableComponent} from './dashboard/advance/advance-table/advance-table.component';
import {AnnouncementTableComponent} from './dashboard/announcement/announcement-table/announcement-table.component';
import {AnnouncementFormComponent} from './dashboard/announcement/announcement-form/announcement-form.component';
import {NewsFormComponent} from './dashboard/news/news-form/news-form.component';
import {NewsTableComponent} from './dashboard/news/news-table/news-table.component';
import {ProfessorTableComponent} from './dashboard/professor/professor-table/professor-table.component';
import {ProfessorFormComponent} from './dashboard/professor/professor-form/professor-form.component';
import {AdminRoleService} from './dashboard/admin-user/admin-role.service';
import {EntityService} from './dashboard/admin-user/entity.service';
import {RoleTypeService} from './dashboard/admin-user/role-type.service';
import {AdminTypeService} from './dashboard/admin-user/admin-type.service';
import {UserService} from './dashboard/user/user.service';
import {ProyectTableComponent} from './dashboard/proyect/proyect-table/proyect-table.component';
import {ProyectFormComponent} from './dashboard/proyect/proyect-form/proyect-form.component';
import {ProyectService} from './dashboard/proyect/proyect.service';
import {ProyectTypeService} from './dashboard/proyect/proyect-type.service';
import {AdvanceService} from './dashboard/advance/advance.service';
import {AnnouncementService} from './dashboard/announcement/announcement.service';
import {EventService} from './dashboard/event/event.service';
import {NewsService} from './dashboard/news/news.service';
import {ProfessorService} from './dashboard/professor/professor.service';
import {AssistantFormComponent} from './dashboard/assistant/assistant-form/assistant-form.component';
import {AssistantTableComponent} from './dashboard/assistant/assistant-table/assistant-table.component';
import {SocialActorFormComponent} from './dashboard/social-actor/social-actor-form/social-actor-form.component';
import {SocialActorTableComponent} from './dashboard/social-actor/social-actor-table/social-actor-table.component';
import {AssistantService} from './dashboard/assistant/assistant.service';
import {SocialActorService} from './dashboard/social-actor/social-actor.service';
import {WorkTeamTableComponent} from './dashboard/work-team/work-team-table/work-team-table.component';
import {WorkTeamFormComponent} from './dashboard/work-team/work-team-form/work-team-form.component';
import {WorkTeamService} from './dashboard/work-team/work-team.service';
import {TimelineComponent} from './dashboard/timeline/timeline.component';
import {HeaderComponent} from './dashboard/header/header.component';
import {SidebarComponent} from './dashboard/sidebar/sidebar.component';
import {WorkTeamProfessorService} from './dashboard/work-team/work-team-professor.service';
import {WorkTeamAssistantService} from './dashboard/work-team/work-team-assistant.service';

@NgModule({
    declarations: [
        AppComponent,
        DashboardComponent,
        LoginComponent,
        NotFoundComponent,
        HeaderComponent,
        SidebarComponent,
        HomeComponent,
        AdminUserTableComponent,
        AdminUserFormComponent,
        UserFormComponent,
        UserTableComponent,
        EventTableComponent,
        EventFormComponent,
        AdvanceFormComponent,
        AdvanceTableComponent,
        AnnouncementTableComponent,
        AnnouncementFormComponent,
        NewsFormComponent,
        NewsTableComponent,
        ProfessorTableComponent,
        ProfessorFormComponent,
        ProyectTableComponent,
        ProyectFormComponent,
        AssistantFormComponent,
        AssistantTableComponent,
        SocialActorFormComponent,
        SocialActorTableComponent,
        WorkTeamTableComponent,
        WorkTeamFormComponent,
        TimelineComponent,

    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        AppRoutingModule,
        NgbDropdownModule.forRoot(),
        NgbCarouselModule.forRoot(),
        NgbAlertModule.forRoot(),
        ToastModule.forRoot(),
    ],
    providers: [
        AdminUserService,
        AdminTypeService,
        AdminRoleService,
        EntityService,
        RoleTypeService,
        UserService,
        ProyectService,
        ProyectTypeService,
        AdvanceService,
        AnnouncementService,
        EventService,
        NewsService,
        ProfessorService,
        AssistantService,
        SocialActorService,
        WorkTeamService,
        WorkTeamProfessorService,
        WorkTeamAssistantService,

    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
