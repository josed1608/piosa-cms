import {Injectable, Input} from '@angular/core';
import {Headers, Http, RequestOptions, Response} from '@angular/http';
import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';
import {AppConstants} from './app-constants';

@Injectable()
export class AdminUserService {

    @Input() selectedObject: any;

    constructor(private http: Http) {
    }

    doLogin(adminUser: any) {
        const headers = new Headers({'Content-Type': 'application/json'});
        return this.http.post(AppConstants.API_ENDPOINT + 'adminUser/login',
            adminUser,
            {headers: headers})
            .map(
                (response: Response) => {
                    const data = response.json();
                    localStorage.setItem('user', JSON.stringify(data));
                    return data;
                }
            )
            .catch(
                (error: Response) => {
                    return Observable.throw(error);
                }
            );
    }

    createAdminUser(adminUser: any) {
        console.log('Create');
        const headers = new Headers({'Content-Type': 'application/json'});

        return this.http.post(AppConstants.API_ENDPOINT + 'adminUser/create',
            adminUser,
            {headers: headers});
    }

    getAdminUsers() {
        console.log('Read');
        return this.http.get(AppConstants.API_ENDPOINT + 'adminUser/findAll')
            .map(
                (response: Response) => {
                    return response.json();
                }
            )
            .catch(
                (error: Response) => {
                    return Observable.throw('Something went wrong' + error);
                }
            );
    }

    getById(objId: string) {
        console.log('Read');
        console.log(objId);
        return this.http.get(AppConstants.API_ENDPOINT + 'adminUser/findById/' + objId)
            .map(
                (response: Response) => {
                    return response.json();
                }
            )
            .catch(
                (error: Response) => {
                    return Observable.throw('Something went wrong' + error);
                }
            );
    }

    getByUser(id: string) {
        console.log('Read');
        return this.http.get(AppConstants.API_ENDPOINT + 'adminUser/findByUser/' + id)
            .map(
                (response: Response) => {
                    return response.json();
                }
            )
            .catch(
                (error: Response) => {
                    return Observable.throw('Something went wrong' + error);
                }
            );
    }


    updateAdminUser(adminUser: any) {
        console.log('Update');
        const headers = new Headers({'Content-Type': 'application/json'});
        return this.http.put(AppConstants.API_ENDPOINT + 'adminUser/update',
            adminUser,
            {headers: headers});
    }

    deleteAdminUser(obj: any) {
        console.log('Delete');
        const headers = new Headers({'Content-Type': 'application/json'});
        return this.http.delete(AppConstants.API_ENDPOINT + 'adminUser/delete',
            new RequestOptions({
                headers: headers,
                body: obj
            }));
    }

}
