//
// Class used to define the url to be used in requests
//

export class AppConstants {

  public static API_ENDPOINT =
  //     'http://13.58.32.238:8081/api/';
  'http://127.0.0.1:8081/api/';

  public static BUCKET_NAME = 'piosa-storage-bucket';


}
