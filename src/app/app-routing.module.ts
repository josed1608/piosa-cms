import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {NotFoundComponent} from './not-found/not-found.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {HomeComponent} from './dashboard/home/home.component';
import {AdminUserTableComponent} from './dashboard/admin-user/admin-user-table/admin-user-table.component';
import {UserTableComponent} from './dashboard/user/user-table/user-table.component';
import {AdvanceTableComponent} from './dashboard/advance/advance-table/advance-table.component';
import {EventTableComponent} from './dashboard/event/event-table/event-table.component';
import {NewsTableComponent} from './dashboard/news/news-table/news-table.component';
import {ProfessorTableComponent} from './dashboard/professor/professor-table/professor-table.component';
import {AnnouncementTableComponent} from './dashboard/announcement/announcement-table/announcement-table.component';
import {AdminUserFormComponent} from './dashboard/admin-user/admin-user-form/admin-user-form.component';
import {UserFormComponent} from './dashboard/user/user-form/user-form.component';
import {AnnouncementFormComponent} from './dashboard/announcement/announcement-form/announcement-form.component';
import {AdvanceFormComponent} from './dashboard/advance/advance-form/advance-form.component';
import {EventFormComponent} from './dashboard/event/event-form/event-form.component';
import {NewsFormComponent} from './dashboard/news/news-form/news-form.component';
import {ProfessorFormComponent} from './dashboard/professor/professor-form/professor-form.component';
import {ProyectTableComponent} from './dashboard/proyect/proyect-table/proyect-table.component';
import {ProyectFormComponent} from './dashboard/proyect/proyect-form/proyect-form.component';
import {AssistantTableComponent} from './dashboard/assistant/assistant-table/assistant-table.component';
import {AssistantFormComponent} from './dashboard/assistant/assistant-form/assistant-form.component';
import {SocialActorTableComponent} from './dashboard/social-actor/social-actor-table/social-actor-table.component';
import {SocialActorFormComponent} from './dashboard/social-actor/social-actor-form/social-actor-form.component';
import {WorkTeamTableComponent} from './dashboard/work-team/work-team-table/work-team-table.component';
import {WorkTeamFormComponent} from './dashboard/work-team/work-team-form/work-team-form.component';

const routes: Routes = [
    {
        path: 'dashboard',
        component: DashboardComponent,
        // canActivate: [AuthGuard],
        children: [
            {path: 'home', component: HomeComponent},

            // Admin User
            {path: 'admin-user', component: AdminUserTableComponent},
            {path: 'admin-user/:id', component: AdminUserFormComponent},

            // Proyect
            {path: 'proyect', component: ProyectTableComponent},
            {path: 'proyect/:id', component: ProyectFormComponent},

            // User
            {path: 'user', component: UserTableComponent},
            {path: 'user/:id', component: UserFormComponent},

            // Announcement
            {path: 'announcement', component: AnnouncementTableComponent},
            {path: 'announcement/:id', component: AnnouncementFormComponent},

            // Advance
            {path: 'advance', component: AdvanceTableComponent},
            {path: 'advance/:id', component: AdvanceFormComponent},

            // Event
            {path: 'event', component: EventTableComponent},
            {path: 'event/:id', component: EventFormComponent},

            // News
            {path: 'news', component: NewsTableComponent},
            {path: 'news/:id', component: NewsFormComponent},

            // Professor
            {path: 'professor', component: ProfessorTableComponent},
            {path: 'professor/:id', component: ProfessorFormComponent},

            // Assistant
            {path: 'assistant', component: AssistantTableComponent},
            {path: 'assistant/:id', component: AssistantFormComponent},

            // Equipo de Trabajo
            {path: 'work-team', component: WorkTeamTableComponent},
            {path: 'work-team/:id', component: WorkTeamFormComponent},

            // Actores Sociales
            {path: 'social-actor', component: SocialActorTableComponent},
            {path: 'social-actor/:id', component: SocialActorFormComponent},

        ]
    },
    {path: 'login', component: LoginComponent},
    {path: 'not-found', component: NotFoundComponent},
    // {path: '', component: LoginComponent},
    {path: '**', redirectTo: 'login'}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
