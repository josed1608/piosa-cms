import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {Router} from '@angular/router';
import {routerTransition} from '../router.animations';
import {AdminUserService} from '../admin-user.service';
import {ToastsManager} from 'ng2-toastr';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {

    loginForm: FormGroup;

    constructor(public router: Router,
                vRef: ViewContainerRef,
                public toastr: ToastsManager,
                private formBuilder: FormBuilder,
                private adminUserService: AdminUserService) {
        this.toastr.setRootViewContainerRef(vRef);
    }

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', Validators.required]
        });

    }


    onLogIn() {
        const object = this.loginForm.value;
        console.log(this.loginForm.value);
        this.adminUserService.doLogin(object)
            .subscribe(
                (response) => {
                    console.log(response);
                    this.router.navigate(['/dashboard']);

                },
                (error) => {
                    console.log(error);
                    const responseStatus = error.status;
                    console.log('resp' + responseStatus);

                    // TODO handle State Message codes
                    if (responseStatus == 300) {
                        this.toastr.error('El correo electrónico no se encuentra registrado',
                            'Ha ocurrido un error');
                    } else if (responseStatus == 301) {
                        this.toastr.error('La contraseña no corresponde con el correo ingresado',
                            'Ha ocurrido un error');
                    } else {
                        this.toastr.error('Contacte a Konrad',
                            'Ha ocurrido un error');
                    }

                }
            );
    }

    // onLoggedin() {
    //     localStorage.setItem('isLoggedin', 'true');
    // }

}
